const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  stock: {
    type: Number,
    default: 1000,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
  imageUrl: {
    type: String,
    default: 'https://i.stack.imgur.com/yZlqh.png',
  },
  rating: {
    type: Number,
    default: 0,
  },
  ratings: [
    {
      stars: {
        type: Number,
        required: true,
      },
      orderId: {
        type: String,
        required: true,
      },
      comment: {
        type: String,
      }
    }
  ],
  numSold: {
    type: Number,
    default: 0,
  },
  slug: String,
});

productSchema.index({ name: 'text', description: 'text' });

module.exports = mongoose.model('Product', productSchema);
