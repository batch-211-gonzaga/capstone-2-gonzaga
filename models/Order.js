const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  products: [
    {
      productId: {
        type: String,
        required: true,
      },
      quantity: {
        type: Number,
        required: true,
      },
      ratedOn: {
        type: Date,
      },
    },
  ],
  totalAmount: Number,
  purchasedOn: Date,
  packedOn: Date,
  shippedOn: Date,
  deliveredOn: Date,
  refundRequestedOn: Date,
  refundApprovedOn: Date,
  returnedOn: Date,
});

module.exports = mongoose.model('Order', orderSchema);
