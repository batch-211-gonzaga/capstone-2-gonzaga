const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  mobileNo: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  address: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  wishlist: [
    {
      productId: {
        type: String,
        required: true,
      },
      addedOn: {
        type: Date,
        default: new Date()
      }
    }
  ]
});

module.exports = mongoose.model('User', userSchema);
