const auth = require('./auth')
const errors = require('./errors');

const makeReturnValue = (data, doReturnData) => {
  let result = {};
  if (doReturnData)
    data = data;
  else
    data = true;

  return data;
};

module.exports.returnTrueOrFalse = (data) => {
  return makeReturnValue(data, error, false);
};

module.exports.returnResultOrFalse = (data) => {
  return makeReturnValue(data, true);
};

module.exports.respondForbidden = (res) => {
  res.status(403);
  res.send(false);
};

module.exports.authenticateAdmin = (req, res) => {
  const isAdmin = module.exports.getAuthUser(req).isAdmin;

  if (!isAdmin)
    module.exports.respondForbidden(res);
  else
    return isAdmin;
};

module.exports.getAuthUser = (req) => {
  return auth.decode(req.headers.authorization);
};

module.exports.reqUserIsAdminOrTarget = (req, targetId) => {
  const user = module.exports.getAuthUser(req);
  if (user.isAdmin || user.id == targetId)
    return true;
  else
    return false;
};

module.exports.handleError = (error, res) => {
  // CastError - probably invalid Id string, respond with 404
  if (error.message == errors.invalidUrlParam) {
    res.status(404)
  }
  else if (error.message == errors.invalidReq) {
    res.status(400)
  }
  // For everything else, there's masterca... code 500
  else {
    res.status(500);
  };

  console.log(error);

  if (!res.headersSent)
    res.send(false);
};

module.exports.slug = (string) => {
  const allowed = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
    'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
    'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
  return Array.from(string).map(c => allowed.includes(c) ? c : '-').join('');
};

module.exports.maxWishItems = 5;
