const express = require('express');
const router = express.Router();
const auth = require('../auth');
const utils = require('../utils');
const orderController = require('../controllers/orderController');
const errors = require('../errors');

router.post('/', auth.verify, (req, res) => {
  const reqUser = utils.getAuthUser(req);

  if (!req.body.userId || utils.reqUserIsAdminOrTarget(req, req.body.userId))
    orderController
      .createClosedOrder(reqUser.id, req.body)
      .then((result) => res.send(result))
      .catch(e => errors.handleOtherErrors(e, res));
  else utils.respondForbidden(res);
});

router.get('/', auth.verify, (req, res) => {
  const reqUser = utils.getAuthUser(req);

  let targetUserId = null;
  if (!reqUser.isAdmin)
    targetUserId = reqUser.id;

  orderController
    .getAllClosed(targetUserId)
    .then((result) => res.send(result))
    .catch(e => errors.handleOtherErrors(e, res));
});

router.get('/user/:userId', auth.verify, (req, res) => {
  const reqUser = utils.getAuthUser(req);
  if (!reqUser.isAdmin)
    utils.respondForbidden(res);

  targetUserId = req.params.userId;

  orderController
    .getAllClosed(targetUserId)
    .then((result) => res.send(result))
    .catch(e => errors.handleOtherErrors(e, res));
});

  //const [packedComponent, setpackedComponent] = useState('');
  //const [shippedComponent, setshippedComponent] = useState('');
  //const [deliveredComponent, setdeliveredComponent] = useState('');
  //const [refundRequestedComponent, setrefundRequestedComponent] = useState('');
  //const [refundApprovedComponent, setrefundApprovedComponent] = useState('');
  //const [returnedComponent, setreturnedComponent] = useState('');

router.post('/:orderId/packed', (req, res) => {
  orderController
    .packed(req.params.orderId)
    .then(result => res.send(result))
    .then(result => res.send(result))
    .catch(e => {
      errors.handleOtherErrors(e, res);
    });
});

router.post('/:orderId/shipped', (req, res) => {
  orderController
    .shipped(req.params.orderId)
    .then(result => res.send(result))
    .then(result => res.send(result))
    .catch(e => {
      errors.handleOtherErrors(e, res);
    });
});

router.post('/:orderId/delivered', (req, res) => {
  orderController
    .delivered(req.params.orderId)
    .then(result => res.send(result))
    .then(result => res.send(result))
    .catch(e => {
      errors.handleOtherErrors(e, res);
    });
});

router.post('/:orderId/refundRequested', (req, res) => {
  orderController
    .refundRequested(req.params.orderId)
    .then(result => res.send(result))
    .then(result => res.send(result))
    .catch(e => {
      errors.handleOtherErrors(e, res);
    });
});

router.post('/:orderId/refundApproved', (req, res) => {
  orderController
    .refundApproved(req.params.orderId)
    .then(result => res.send(result))
    .then(result => res.send(result))
    .catch(e => {
      errors.handleOtherErrors(e, res);
    });
});

router.post('/:orderId/returned', (req, res) => {
  orderController
    .returned(req.params.orderId)
    .then(result => res.send(result))
    .then(result => res.send(result))
    .catch(e => {
      errors.handleOtherErrors(e, res);
    });
});

router.get('/:orderId', auth.verify, (req, res) => {
  orderController
    .getOrder(req.params.orderId)
    .then((result) => {
      const targetUserId = result.userId;

      if (
        utils.reqUserIsAdminOrTarget(req, targetUserId) ||
        !result // Order does not exist - will 404
      ) {
        res.send(result);
      } else {
        res.status(403);
        res.send(false);
      }
    })
    .catch((e) => {
      errors.handleBadOrderInUrl(e, res);
      errors.handleOtherErrors(e, res);
    })
});

module.exports = router;
