const express = require('express');
const router = express.Router();
const auth = require('../auth');
const utils = require('../utils');
const userController = require('../controllers/userController');
const errors = require('../errors');

const adminEmails = ['admin@mail.com'];

router.post('/login', (req, res) => {
  if (!req.body.email || !req.body.password) {
    res.status(401);
    res.send(false);
    return;
  }

  userController.login(req.body).then(token => {
    if (token)
      res.send(token);
    else {
      res.status(401);
      res.send(false);
    }
  })
  .catch(e => utils.handleOtherErrors(e, res));
});

router.post('/checkEmail', (req, res) => {
  userController
    .checkIfEmailExists(req.body.email)
    .then(result => res.send(result));
});

router.post('/', (req, res) => {
  const newUserData = req.body;

  userController
    .checkIfEmailExists(newUserData.email)
    .then(emailExists => {
      if (emailExists) {
        res.status(409);
        res.send(false);
        return null;
      }

      if (adminEmails.includes(newUserData.email))
        newUserData.isAdmin = true;
      else
        newUserData.isAdmin = false;

      return newUserData;

    })
    .then(newUserData => {
      if (newUserData)
        userController.createUser(newUserData)
          .then(newUser => res.send(newUser))
          .catch(e => utils.handleOtherErrors(e, res));
    });
});

router.get('/', auth.verify, (req, res) => {
  if (utils.authenticateAdmin(req, res))
    userController.getAll()
      .then(result => res.send(result))
      .catch(e => utils.handleOtherErrors(e, res));
});

router.patch('/:userId', auth.verify, (req, res) => {
  const targetUserId = req.params.userId;
  const userData = req.body;
  if (utils.reqUserIsAdminOrTarget(req, targetUserId))
    userController.updateUser(targetUserId, userData)
    .then(result => res.send(result))
    .catch(e => {
      errors.handleBadUserInUrl(e, res) ||
      errors.handleOtherErrors(e, res);
    });

});

router.patch('/:userId/makeadmin', auth.verify, (req, res) => {
  if (utils.authenticateAdmin(req, res))
    userController.makeAdmin(req.params.userId)
    .then(result => res.send(result))
    .catch(e => {
      errors.handleBadUserInUrl(e, res) ||
      errors.handleOtherErrors(e, res);
    });

});

router.get('/:userId', auth.verify, (req, res) => {
  const targetId = req.params.userId;

  if (utils.reqUserIsAdminOrTarget(req, targetId)) {
    userController.getUser(targetId)
      .then(user => res.send(user))
      .catch(e => {
        errors.handleBadUserInUrl(e, res) ||
        errors.handleOtherErrors(e, res);
      });
  } else {
    res.status(403);
    res.send(false);
  }
});

module.exports = router;
