const express = require('express');
const router = express.Router();
const auth = require('../auth');
const utils = require('../utils');
const orderController = require('../controllers/orderController');
const cartController = require('../controllers/cartController');
const userController = require('../controllers/userController');
const errors = require('../errors');

router.get('/', auth.verify, (req, res) => {
  const userId = utils.getAuthUser(req).id;
  cartController
    .getClientFriendlyCart(userId)
    .then((result) => res.send(result))
    .catch((e) => errors.handleOtherErrors(e, res));
});

router.post('/', auth.verify, (req, res) => {
  const userId = utils.getAuthUser(req).id;
  const productId = req.body.productId;
  const quantity = req.body.quantity;

  if (!productId) {
    res.status(400);
    res.send(false);
    return;
  } else {
    cartController
      .setProductQuantity(userId, productId, quantity)
      .then((result) => res.send(result))
      .catch((e) => {
        errors.handleDataStateConflict(e, res) ||
        errors.handleBadProductInReq(e, res) ||
        errors.handleOtherErrors(e, res);
      });
  }
});

router.post('/empty', auth.verify, (req, res) => {
  const userId = utils.getAuthUser(req).id;
  cartController
    .clearCart(userId)
    .then((cart) => res.send(cart))
    .catch((e) => errors.handleOtherErrors(e, res));
});

router.post('/checkout', auth.verify, (req, res) => {
  const userId = utils.getAuthUser(req).id;
  cartController
    .checkout(userId)
    .then((cart) => res.send(cart))
    .catch((e) => errors.handleOtherErrors(e, res));
});

module.exports = router;
