const express = require('express');
const router = express.Router();
const auth = require('../auth');
const utils = require('../utils');
const productController = require('../controllers/productController');
const errors = require('../errors');

router.post('/', auth.verify, (req, res) => {
  if (utils.authenticateAdmin(req, res))
    productController.createProduct(req.body)
    .then(result => res.send(result))
    .catch(e => utils.handleOtherErrors(e, res));
});

router.get('/search/:term', (req, res) => {
  productController
    .search(req.params.term)
    .then(result => res.send(result))
    .catch(e => {
      errors.handleOtherErrors(e, res);
    });
});

router.get('/active', (req, res) => {
  productController.getActiveProducts()
    .then(result => res.send(result))
});

router.get('/', (req, res) => {
  productController.getAllProducts()
    .then(result => res.send(result))
    .then(result => res.send(result))
    .catch(e => {
      errors.handleOtherErrors(e, res);
    });
});

router.get('/slug/:slug', (req, res) => {
  productController.getProductFromSlug(req.params.slug)
    .then(result => res.send(result))
    .then(result => res.send(result))
    .catch(e => {
      errors.handleOtherErrors(e, res);
    });
});

router.get('/:productId', (req, res) => {
  productController.getProduct(req.params.productId)
    .then(result => res.send(result))
    .catch(e => {
      errors.handleBadProductInUrl(e, res) ||
      errors.handleOtherErrors(e, res);
    });
});

router.post('/:productId/archive', auth.verify, (req, res) => {
  if (utils.authenticateAdmin(req, res))
    productController.disableProduct(req.params.productId)
      .then(result => res.send(result))
      .catch(e => {
        errors.handleBadProductInUrl(e, res) ||
        errors.handleOtherErrors(e, res);
      });
});

router.post('/:productId/activate', auth.verify, (req, res) => {
  if (utils.authenticateAdmin(req, res))
    productController.enableProduct(req.params.productId)
      .then(result => res.send(result))
      .catch(e => {
        errors.handleBadProductInUrl(e, res) ||
        errors.handleOtherErrors(e, res);
      });
});

router.post('/:productId/wish', auth.verify, (req, res) => {
  const userId = utils.getAuthUser(req).id;
  if (userId)
    productController.wish(req.params.productId, userId)
      .then(result => res.send(result))
      .catch(e => {
        console.log(e);
        errors.handleDataStateConflict(e, res) ||
        errors.handleAlreadyInWishlist(e, res) ||
        errors.handleWishLimitExceed(e, res) ||
        errors.handleOtherErrors(e, res);
      });
  else
    res.send(false);
});

router.post('/:productId/unwish', auth.verify, (req, res) => {
  const userId = utils.getAuthUser(req).id;
  if (userId)
    productController.unwish(req.params.productId, userId)
      .then(result => res.send(result))
      .catch(e => {
        errors.handleDataStateConflict(e, res) ||
        errors.handleOtherErrors(e, res);
      });
  else
    res.send(false);
});

router.post('/:productId/rate', auth.verify, (req, res) => {
  const productId = req.params.productId;
  const stars = req.body.stars;
  const comment = req.body.comment;
  const orderId = req.body.orderId;
  productController
    .rate(productId, orderId, stars, comment)
    .then(result => res.send(result))
    .catch(e => errors.handleOtherErrors(e, res));
});

//router.post('/:productId/addSold', auth.verify, (req, res) => {
  //const productId = req.params.productId;
  //const amount = req.body.amount;
  //productController
    //.addSold(productId, amount)
    //.then(result => res.send(result));
//});

router.patch('/:productId', auth.verify, (req, res) => {
  if (utils.authenticateAdmin(req, res))
    productController.updateProduct(req.params.productId, req.body)
      .then(result => res.send(result))
      .catch(e => {
        errors.handleBadProductInUrl(e, res) ||
        errors.handleOtherErrors(e, res);
      });
});

module.exports = router;
