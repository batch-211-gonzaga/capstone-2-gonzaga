const utils = require('./utils');

module.exports.badUser = 'User Id does not exist in database';
module.exports.badProduct = 'Product Id does not exist in database';
module.exports.badOrder = 'Order Id does not exist in database';
module.exports.dataStateConflit = 'Data state conflict';
module.exports.wrongPassword = 'Wrong password';
module.exports.wishLimitExceeded = 'Wish limit exceeded';
module.exports.alreadyInWishlist = 'Already in wishlist';

module.exports.handleBadUserInUrl = (e, res) => {
  if (e == module.exports.badUser) {
    console.error(e);
    res.status(404);
    if (!res.headersSent)
      res.send(false);
    return e;
  }
};

module.exports.handleBadUserInReq = (e, res) => {
  if (e == module.exports.badUser) {
    console.error(e);
    res.status(422);
    if (!res.headersSent)
      res.send(false);
    return e;
  }
};

module.exports.handleBadProductInUrl = (e, res) => {
  if (e == module.exports.badProduct) {
    console.error(e);
    res.status(404);
    if (!res.headersSent)
      res.send(false);
    return e;
  }
};

module.exports.handleBadProductInReq = (e, res) => {
  if (e == module.exports.badProduct) {
    console.error(e);
    res.status(422);
    if (!res.headersSent)
      res.send(false);
    return e;
  }
};

module.exports.handleBadOrderInUrl = (e, res) => {
  if (e == module.exports.badOrder) {
    console.error(e);
    res.status(404);
    if (!res.headersSent)
      res.send(false);
    return e;
  }
};

module.exports.handleBadOrderInReq = (e, res) => {
  if (e == module.exports.badOrder) {
    console.error(e);
    res.status(422);
    if (!res.headersSent)
      res.send(false);
    return e;
  }
};

module.exports.handleOtherErrors = (e, res) => {
  if (e) {
    console.error(e);
    res.status(500);
    if (!res.headersSent)
      res.send(false);
    return e;
  }
};

module.exports.handleDataStateConflict = (e, res) => {
  if (e == module.exports.dataStateConflit) {
    console.error(e);
    res.status(409);
    if (!res.headersSent)
      res.send(false);
    return e;
  } else
    return null;
};

module.exports.handleWishLimitExceed = (e, res) => {
  console.log(e);
  if (e == module.exports.wishLimitExceeded) {
    console.error(e);
    res.status(409);
    if (!res.headersSent)
      res.send(JSON.stringify({ message: `Wish limit exceeded. Can only wish up to ${utils.maxWishItems} items.` }));
    return e;
  } else
    return null;
};

module.exports.handleAlreadyInWishlist = (e, res) => {
  console.log(e);
  if (e == module.exports.alreadyInWishlist) {
    console.error(e);
    res.status(409);
    if (!res.headersSent)
      res.send(JSON.stringify({ message: 'Product already in wishlist.' }));
    return e;
  } else
    return null;
};

module.exports.catchCastErrorAndThrow = (e, eToThrow) => {
  if (e.name == 'CastError' && e.kind == 'ObjectId') {
    console.error(e);
    throw eToThrow;
  } else {
    throw e;
  }
};

