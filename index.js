const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');
const cartRoutes = require('./routes/cartRoutes');

const app = express();

const PORT = process.env.PORT || 4000;

// MongoDB
mongoose.connect(
  'mongodb+srv://admin123:admin123@project0.csykcns.mongodb.net/c2?retryWrites=true&w=majority',
  {
    useNewUrlParser: true, // compatibility
    useUnifiedTopology: true, // compatibility
  }
);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB error'));
db.once('open', () => console.log('Connected to MongoDB'));

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// routes
const homeMessage = "Hello, this is the API for my e-commerce project."
const homeRoute = express.Router().get('/', (req, res) => {
  res.send(homeMessage);
});

app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);
app.use('/cart', cartRoutes);
app.use('/', homeRoute);

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
