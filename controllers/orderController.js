const Order = require('../models/Order');
const User = require('../models/User');
const userController = require('../controllers/userController');
const productController = require('./productController');
const Product = require('../models/Product')
const utils = require('../utils');
const errors = require('../errors');

module.exports.calculateTotalAmount = async (products) => {
  let total = 0;
  for (let i = 0; i < products.length; i++) {
    await Product.findById(products[i].productId).then(product => {
      if (!product)
        return;

      let price = product.price;
      total += product.price * products[i].quantity;
    });
  }
  return total;
};

// createClosedOrder
module.exports.createClosedOrder = async (reqUserId, orderData) => {
  const newOrderData = {};
  // delete invalid product ids, inactive products
  for (let i = 0; i < orderData.products.length; i++) {
    const p = await Product.findById(orderData.products[i].productId)
      .catch(e => console.log('createClosedOrder:verify productId', e));

    if (!p || !p.isActive || orderData.products[i].quantity < 1)
      delete orderData.products[i];
    else {
      productController.addSold(orderData.products[i].productId,
                                orderData.products[i].quantity);
    }
  };

  const newOrder = new Order(orderData);

  if (!newOrder.userId)
    newOrder.userId = reqUserId;

  return module.exports.calculateTotalAmount(newOrder.products)
    .then(total => {
      newOrder.totalAmount = total;
      newOrder.purchasedOn = orderData.purchasedOn || new Date();
      return newOrder.save().then(utils.returnResultOrFalse);
    })
};


// getAllClosed
module.exports.getAllClosed = (userId) => {
  let filter = { purchasedOn: { '$ne': null }};
  if (userId)
    filter.userId = userId;

  return Order.find(filter, '-__v')
    .then((res) => {

      return utils.returnResultOrFalse(res);
    });
};

// getOrder by id
module.exports.getOrder = (orderId) => {
  return Order.findById(orderId, '-__v')
    .then((result) => {
      if (!result)
        throw errors.badOrder;

      return utils.returnResultOrFalse(result);
    })
    .catch(e => errors.catchCastErrorAndThrow(e, errors.badOrder));
};

// addProduct
module.exports.addProduct = (productId, quantityToAdd) => {
  if (!productController.checkIfProductExists(productId))
    return utils.returnTrueOrFalse(false);

  if (!quantityToAdd)
    quantityToAdd = 1;

  const order = Order.findById(productId)
    .then(utils.returnResultOrFalse);
};

// packed
module.exports.packed = (orderId) => {
  return Order.findByIdAndUpdate (
    orderId,
    {packedOn: new Date()},
    {returnDocument: 'after'}
  )
};

// shipped
module.exports.shipped = (orderId) => {
  return Order.findByIdAndUpdate (
    orderId,
    {shippedOn: new Date()},
    {returnDocument: 'after'}
  )
};

// delivered
module.exports.delivered = (orderId) => {
  return Order.findByIdAndUpdate (
    orderId,
    {deliveredOn: new Date()},
    {returnDocument: 'after'}
  )
};

// refundRequested
module.exports.refundRequested = (orderId) => {
  return Order.findByIdAndUpdate (
    orderId,
    {refundRequestedOn: new Date()},
    {returnDocument: 'after'}
  )
};

// refundApproved
module.exports.refundApproved = (orderId) => {
  return Order.findByIdAndUpdate (
    orderId,
    {refundApprovedOn: new Date()},
    {returnDocument: 'after'}
  )
};

// returned
module.exports.returned = (orderId) => {
  return Order.findByIdAndUpdate (
    orderId,
    {returnedOn: new Date()},
    {returnDocument: 'after'}
  )
};
