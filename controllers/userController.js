const User = require('../models/User');
const auth = require('../auth');
const bcrypt = require('bcrypt');
const utils = require('../utils');
const errors = require('../errors');

// checkIfEmailExists
module.exports.checkIfEmailExists = (email) => {
  return User.find({ email: email }).then((result) => {
    if (result.length > 0)
      return true;
    else
      return false;
  });
};

// checkIfUserIdExists
module.exports.checkIfUserIdExists = (userId) => {
  return User.findById(userId).then((result) => {
    if (result)
      return true;
    else
      return false;
  });
};

// createUser
module.exports.createUser = (userData) => {
  const newUser = new User(userData);

  newUser.password = bcrypt.hashSync(userData.password, 10);

  return newUser.save().then((user) => {
    user = user.toObject();
    delete user.__v;
    delete user.password;

    return user;
  });
};

// login
module.exports.login = (credentials) => {
  return User.findOne({ email: credentials.email }).then((user) => {
      if (!user)
        return null;

    const isPasswordCorrect = bcrypt.compareSync(credentials.password, user.password);

    if (isPasswordCorrect)
      return { access: auth.createAccessToken(user), id: user._id };
    else
      return null;
  });
};

// getUser
module.exports.getUser = (userId) => {
  return User.findById(userId, '-password -__v')
    .then((user) => {
      if (!user)
        throw errors.badUser;
      else {
        return user;
      }
    })
    .catch(e => errors.catchCastErrorAndThrow(e, errors.badUser));
};

// updateUser
module.exports.updateUser = (userId, userData) => {
  if (userData.password)
    userData.password = bcrypt.hashSync(userData.password, 10);

  return User.findByIdAndUpdate(userId, userData, { returnDocument: 'after' })
};

// makeAdmin
module.exports.makeAdmin = (userId) => {
  return User.findByIdAndUpdate(userId, { isAdmin: true }, { returnDocument: 'after' })
    .then((user) => {
      if (!user)
        throw errors.badUser;

      user = user.toObject();
      delete user.password;

      return user;
    })
    .catch(e => errors.catchCastErrorAndThrow(e, errors.badUser));
};

//// getAll
module.exports.getAll = () => {
  return User.find({}, '-password -__v').then(utils.returnResultOrFalse);
};

