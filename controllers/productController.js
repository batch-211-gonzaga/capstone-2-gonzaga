const Product = require('../models/Product');
const utils = require('../utils');
const errors = require('../errors');
const User = require('../models/User');
const Order = require('../models/Order')

// checkIfProductExists
module.exports.checkIfProductExists = (productId) => {
  return Product.find({ _id: productId }).then((result) => {
    if (result.length > 0)
      return true;
    else
      return false;
})};

// createProduct
module.exports.createProduct = async (productData) => {
  const newProduct = new Product(productData);
  await newProduct.save();
  newProduct.slug = utils.slug(`${newProduct.name} ${newProduct._id}`)
  return newProduct.save();
};

// getActiveProducts
module.exports.getActiveProducts = () => {
  return Product.find({ isActive: true }, '-__v');
};

// getAll
module.exports.getAllProducts = () => {
  return Product.find({}, '-__v');
};

// getProduct by id
module.exports.getProduct = (productId) => {
  return Product.findById(productId, '-__v')
    .then((product) => {
      if (!product)
        throw errors.badProduct;
      else
        return product;
    })
    .catch(e => errors.catchCastErrorAndThrow(e, errors.badProduct));
};

// updateProduct
module.exports.updateProduct = (productId, newData) => {
  return Product.findByIdAndUpdate(productId, newData, { returnDocument: 'after' })
    .then(product => {
      if (!product)
        throw errors.badProduct;
      else
        return product;
    })
    .catch(e => errors.catchCastErrorAndThrow(e, errors.badProduct));
};

// disableProduct
module.exports.disableProduct = (productId) => {
  return Product.findByIdAndUpdate(productId, { isActive: false }, { returnDocument: 'after' })
    .then(product => {
      if (!product)
        throw errors.badProduct;
      else
        return product;
    })
    .catch(e => errors.catchCastErrorAndThrow(e, errors.badProduct));
};

// enableProduct
module.exports.enableProduct = (productId) => {
  return Product.findByIdAndUpdate(productId, { isActive: true }, { returnDocument: 'after' })
    .then(product => {
      if (!product)
        throw errors.badProduct;
      else
        return product;
    })
    .catch(e => errors.catchCastErrorAndThrow(e, errors.badProduct));
};

// wish
module.exports.wish = (productId, userId) => {
  return User
    .findById(userId)
    .then(user => {
      if (!user)
        return false;

      const wishProdIds = user.wishlist.map(i => i.productId);

      if (wishProdIds.includes(productId))
        throw errors.alreadyInWishlist;

      if (wishProdIds.length >= utils.maxWishItems)
        throw errors.wishLimitExceeded;

      user.wishlist.push({ productId: productId });
      return user.save();
    });
};

// unwish
module.exports.unwish = (productId, userId) => {
  return User
    .findById(userId)
    .then(user => {
      if (!user)
        return false;

      const wishProdIds = user.wishlist.map(i => i.productId);
      if (wishProdIds.includes(productId)) {
        user.wishlist.splice(wishProdIds.indexOf(productId), 1);
        return user.save();
      } else
        throw errors.dataStateConflit;
    });
};

module.exports.rate = (productId, orderId, stars, comment) => {
  return Product
    .findById(productId)
      .then(product => {
        if (!product)
          return false;

        const ratingOrderIds = product.ratings.map(r => r.orderId);
        if (ratingOrderIds.includes(orderId))
          return false;

        product.ratings.push({
          stars: stars,
          orderId: orderId,
          comment: comment,
        });

        const starsList = product.ratings.map(r => r.stars);
        product.rating = starsList.reduce((a, c) => a + c) / starsList.length;

        return product.save();
      })
      .then(product => {
        if (!product)
          return false;

        return Order.findById(orderId)
          .then(order => {

            let productIndex;
            for (let i = 0; i < order.products.length; i++) {
              if (order.products[i].productId == productId) {
                productIndex = i;
              }
            }

            if (order.products[productIndex])
              order.products[productIndex].ratedOn = new Date();

            const ratedOn = order.products[productIndex].ratedOn;

            const rating =  product.ratings.filter(rating => {
                const bo = rating.orderId === order._id.toString();
                return bo;
              })[0];

            order.save();
            return {ratedOn: ratedOn, stars: rating.stars, comment: rating.comment}
          })
      });
};

module.exports.addSold = (productId, quantity) => {
  return Product
    .findById(productId)
    .then(product => {
      product.numSold += quantity;
      product.stock -= quantity;

      return product.save();
    });
};

module.exports.search = (term) => {
  console.log('######## doing search...##########')
  return Product
    .find({ $text: { $search: decodeURIComponent(term) } })
    .exec();
};

module.exports.getProductFromSlug = (slug) => {
  return Product.findOne({ slug: slug });
};
