const Order = require('../models/Order');
const User = require('../models/User');
const userController = require('../controllers/userController');
const productController = require('./productController');
const orderController = require('./orderController');
const Product = require('../models/Product');
const utils = require('../utils');
const errors = require('../errors');

//// Cart operations - a cart is an order with a null purchasedOn field ////

// createCart
module.exports.createCart = (userId) => {
  const newOrder = new Order();
  newOrder.userId = userId;
  newOrder.totalAmount = 0;
  return newOrder.save().then(utils.returnResultOrFalse);
};

const formatClientFriendlyCart = async order => {
    const newProdArray = [];
    for (let i = 0; i < order.products.length; i++) {
      const newProd = {};
      let p = await Product.findById(order.products[i].productId, '-__v');
      newProd.name = p.name;
      newProd.description = p.description;
      newProd.productId = p._id;
      newProd.price = p.price;
      newProd.quantity = order.products[i].quantity;
      newProdArray.push(newProd);
    }
    order = order.toObject();
    order.products = newProdArray;
    order.orderId = order._id;
    delete order.__v;
    return order;
};

// getCart
module.exports.getClientFriendlyCart = (userId) => {

  return Order.findOne({ userId: userId, purchasedOn: null })
    .then((order) => {
      if (!order)
        return module.exports.createCart(userId)
          .then(utils.returnResultOrFalse);
      else
        return order;
    })
    .then(formatClientFriendlyCart);
};

// getCart
module.exports.getCart = (userId) => {
  return Order.findOne({ userId: userId, purchasedOn: null })
    .then((order, error) => {
      if (!order)
        return module.exports.createCart(userId)
          .then(utils.returnResultOrFalse);
      else
        return utils.returnResultOrFalse(order, error);
    });
};

// setProductQuantity
module.exports.setProductQuantity = (userId, productId, quantity) => {
  console.log(`setProductQuantity:: userId: ${userId}, productId:${productId}, quantity: ${quantity}`);
  return module.exports.getCart(userId).then((cart) => {
    return Product.findById(productId).then(async (product) => {
      if (!product)
        throw errors.badProduct;

      if (!product.isActive)
        throw errors.dataStateConflit;

      let cartProduct = null;

      cart.products.forEach((p) => {
        if (p.productId == productId)
          cartProduct = p;
      });

      if (cartProduct && quantity === undefined)
        cartProduct.quantity++;

      if (cartProduct && quantity > 0)
        cartProduct.quantity = quantity;

      if (cartProduct && quantity <= 0) {
        const idx = cart.products.indexOf(cartProduct);
        cart.products.splice(idx, 1);
      }

      if (!cartProduct && quantity > 0)
        cart.products.push({ productId: productId, quantity: quantity });

      if (!cartProduct && quantity === undefined)
        cart.products.push({ productId: productId, quantity: 1 });

      cart.totalAmount = await orderController.calculateTotalAmount(cart.products);
      cart.save();
      return cart;
    })
    .catch((e) => errors.catchCastErrorAndThrow(e, errors.badProduct));
  });
};

// clearCart
module.exports.clearCart = (userId) => {
  return module.exports
    .getCart(userId).then(cart => {
      cart.products = [];
      cart.totalAmount = 0;
      return cart.save()
  });
};

// checkout
module.exports.checkout = async (userId) => {
  return module.exports.getCart(userId).then(cart => {
    if (cart.products.length == 0)
      return cart;
    else {
      cart.purchasedOn = new Date();

      for (let i = 0; i < cart.products.length; i++) {
        const productId = cart.products[i].productId;
        const quantity = cart.products[i].quantity;

        productController.addSold(productId, quantity);
      }
      return cart.save();
    }
  })
  .then(order => {
    const o = formatClientFriendlyCart(order)
    o.purchasedOn = order.purchasedOn
    return o;
  });
};
